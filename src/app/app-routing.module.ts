import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { SpellsComponent } from './components/spells/spells.component';

const routes: Route[] = [
  {
    path: 'spells',
    component: SpellsComponent
  },
  {
    path: '',
    redirectTo: 'spells',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
})
export class AppRoutingModule { }
