import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { SpellsComponent } from './components/spells/spells.component';
import { ApiService } from './services/api.service';

@NgModule({
  declarations:[ AppComponent, SpellsComponent],
  imports: [ BrowserModule, AppRoutingModule, ReactiveFormsModule, HttpClientModule ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
