import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged, Observable, startWith, switchMap } from 'rxjs';
import { Spell } from 'src/app/entities/spell';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-spells',
  templateUrl: './spells.component.html',
  styleUrls: ['./spells.component.scss']
})
export class SpellsComponent implements OnInit {

  spells$: Observable<Spell[]>;

  spellsFormGroup: FormGroup = new FormGroup({
    name: new FormControl('')
  });

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    const nameValue: FormControl = this.spellsFormGroup.get('name') as FormControl;

    

    this.spells$ = nameValue.valueChanges.pipe(
      startWith('B'),
      debounceTime(700),
      distinctUntilChanged(),
      switchMap((search: string)=> this.apiService.getSpells(search))
    );

  }

}
