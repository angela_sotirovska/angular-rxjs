export interface Spell {
    id: number,
    name: string,
    incantation: string, 
    effect: string
}
