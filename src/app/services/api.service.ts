import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, startWith } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Spell } from '../entities/spell';

@Injectable()
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  getSpells(name: string): Observable<Spell[]>{

    const options = {
      params: {
        Name: name
      }
    }

    return this.httpClient.get(environment.apiUrl, options).pipe(
      map((spells: any) => {
        return spells.map((spell: any) => {
          return {
            id: spell.id,
            name: spell.name,
            incantation: spell.incantation,
            effect: spell.effect
          }
        })
      })
    )

  }
}